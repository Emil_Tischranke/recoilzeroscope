// Variables to store max values
int unsigned topDiffMax;
int unsigned bottomDiffMax;
int unsigned leftDiffMax;
int unsigned rightDiffMax;

// Timing values
const unsigned long ledTime = 1000;
unsigned long hitTime = 0;
unsigned int hit = 0;

// Diff tolerance between top/bottom and left/right sensors for zeroed scope (10 ~ 1% tolerance)
const int tolerance = 10;
const int activation_tolerance = 50;

void setup()
{
  Serial.begin(9600);
  pinMode(13, OUTPUT); // Top LED
  pinMode(12, OUTPUT); // Bottom LED
  pinMode(11, OUTPUT); // Left LED
  pinMode(10, OUTPUT); // Right LED

  digitalWrite(10, LOW); // Right LED off
  digitalWrite(11, LOW); // Left LED off
  digitalWrite(12, LOW); // Bottom LED off
  digitalWrite(13, LOW); // Top LED off  
}

void loop() {
  // Set currentTime
  unsigned long currentTime = millis();
  
  // Constantly read four sensor values
  int topSensorValue = analogRead(0);
  int bottomSensorValue = analogRead(1);
  int leftSensorValue = analogRead(2);
  int rightSensorValue = analogRead(3);  

  // Remove comment for plotter debug
  
  Serial.print("hit: "); Serial.print(hit); Serial.print(" "); Serial.print(topSensorValue); Serial.print(" "); Serial.print(bottomSensorValue); Serial.print(" "); Serial.print(leftSensorValue); Serial.print(" "); Serial.println(rightSensorValue);

  // If top/bottom hit is recognized
  if (topSensorValue > activation_tolerance || bottomSensorValue > activation_tolerance){
 
    // Find and set max values per Sensor
    if (topDiffMax < topSensorValue)
    {
      topDiffMax = topSensorValue;
    }
    if (bottomDiffMax < bottomSensorValue)
    {
      bottomDiffMax = bottomSensorValue;
    }

    //  Set top or bottom LED if signal strength is larger than tolerance
    if (topDiffMax > bottomDiffMax + tolerance)
    {
      digitalWrite(13, HIGH);
      digitalWrite(12, LOW);
      //Serial.println("top");
    }
    else if (bottomDiffMax > topDiffMax + tolerance)
    {
      digitalWrite(13, LOW);
      digitalWrite(12, HIGH);
      //Serial.println("bottom");
    }
    else
    {
      digitalWrite(13, HIGH);
      digitalWrite(12, HIGH);
      //Serial.println("top/bottom adjusted");     
    }
    hitTime = currentTime;
    hit = 1;
  }

  // If left/right hit is recognized
  if (leftSensorValue > activation_tolerance || rightSensorValue > activation_tolerance){
 
    // Find and set max values per Sensor
    if (leftDiffMax < leftSensorValue)
    {
      leftDiffMax = leftSensorValue;
    }
    if (rightDiffMax < rightSensorValue)
    {
      rightDiffMax = rightSensorValue;
    }

    //  Set left or right LED if signal strength is larger than tolerance
    if (leftDiffMax > rightDiffMax + tolerance)
    {
      digitalWrite(11, HIGH);
      digitalWrite(10, LOW);
      //Serial.println("left");
    }
    else if (rightDiffMax > leftDiffMax + tolerance)
    {
      digitalWrite(11, LOW);
      digitalWrite(10, HIGH);
      //Serial.println("right");
    }
    else if (rightDiffMax < leftDiffMax + tolerance || leftDiffMax < rightDiffMax + tolerance)
    {
      digitalWrite(11, HIGH);
      digitalWrite(10, HIGH);
      //Serial.println("left/right adjusted");
    }
    hitTime = currentTime;
    hit = 1;
  }

  // If ledTime since hit is reached, reset everything
  if ( currentTime - hitTime >= ledTime && hit == 1 )
  {
    digitalWrite(10, LOW); // Right LED off
    digitalWrite(11, LOW); // Left LED off
    digitalWrite(12, LOW); // Bottom LED off
    digitalWrite(13, LOW); // Top LED off  

    // Remove comment for serial debug of max values
    Serial.print("Top: "); Serial.print(topDiffMax); Serial.print(" Bottom: "); Serial.print(bottomDiffMax); Serial.print(" Left: "); Serial.print(leftDiffMax); Serial.print(" Right: "); Serial.println(rightDiffMax);

    // Reset Max Values
    topDiffMax = 0;
    bottomDiffMax = 0;
    leftDiffMax = 0;
    rightDiffMax = 0;
    hit = 0;
  }
}
